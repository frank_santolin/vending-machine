/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine;

import static machine.model.Coin.sum;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import machine.model.Coin;
import machine.util.MoneyAmount;

public final class MockCoinDispenser implements CoinDispenser {

  private List<Coin> coins = new ArrayList<>();

  @Override
  public void accept(final List<Coin> coins) {
    this.coins.addAll(coins);
  }

  public void assertIsEmpty() {
    assertTrue(coins.isEmpty());
  }

  public void assertCointainsCoinsToTheAmountOf(final MoneyAmount amount) {
    if (amount == null) {
      fail("money amount is null");
    }

    assertCointainsCoinsToTheAmountOf(amount.getValue());
  }

  public void assertCointainsCoinsToTheAmountOf(final double amount) {
    assertThat(sum(coins), equalTo(amount));
  }

}
