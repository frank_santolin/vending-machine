/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import machine.util.MoneyAmount;

public final class MockDisplay implements Display {

  private StringBuilder buffer = new StringBuilder();

  @Override
  public void accept(final String message) {
    buffer.setLength(0);
    buffer.append(message);
  }

  public void show(final String message) {
    accept(message);
  }

  public void assertDisplayShows(final String expectedMessage) {
    if (expectedMessage == null) {
      fail("expected message is null");
    }

    assertThat(buffer.toString(), containsString(expectedMessage));
  }

  public void assertDisplayShowsAmount(final MoneyAmount amount) {
    if (amount == null) {
      fail("money amount is null");
    }

    assertDisplayShowsAmount(amount.getValue());
  }

  public void assertDisplayShowsAmount(final double amount) {
    assertDisplayShows(Display.formatInsertedMoneyAmount(amount));
  }

}
