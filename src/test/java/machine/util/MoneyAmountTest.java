/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine.util;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import machine.util.MoneyAmount;

public class MoneyAmountTest {

  @Test
  public void zeroAmount() {
    MoneyAmount zeroAmount = MoneyAmount.zero();

    assertThat(zeroAmount.getValue(), equalTo(0.00));
  }

  @Test
  public void validStringAmounts() {
    assertThat(MoneyAmount.of("£0.10").getValue(), equalTo(0.10));
    assertThat(MoneyAmount.of("£0.20").getValue(), equalTo(0.20));
    assertThat(MoneyAmount.of("0.50").getValue(), equalTo(0.50));
    assertThat(MoneyAmount.of("0.5").getValue(), equalTo(0.50));
    assertThat(MoneyAmount.of("£1.00").getValue(), equalTo(1.00));
    assertThat(MoneyAmount.of("-0.10").getValue(), equalTo(-0.10));
    assertThat(MoneyAmount.of("£-1.89").getValue(), equalTo(-1.89));
  }

  @Test
  public void validDoubleAmounts() {
    assertThat(MoneyAmount.of(0.10).getValue(), equalTo(0.10));
  }

  @Test
  public void invalidStringAmounts() {
    assertThat(MoneyAmount.of("?.10").getValue(), equalTo(Double.NaN));
    assertThat(MoneyAmount.of("ten pounds").getValue(), equalTo(Double.NaN));
    assertThat(MoneyAmount.of((String) null).getValue(), equalTo(Double.NaN));
  }

  @Test
  public void invalidDoubleAmounts() {
    assertThat(MoneyAmount.of((Double) null).getValue(), equalTo(Double.NaN));
  }

  @Test
  public void isThisAmountLessThanGivenAmount() {
    assertFalse(MoneyAmount.of(1.70).isLessThan(1.70));
    assertFalse(MoneyAmount.of(1.70).isLessThan(null));
    assertTrue(MoneyAmount.of(0.70).isLessThan(1.20));
    assertFalse(MoneyAmount.of(3.70).isLessThan(new Double("3.70")));
  }

  @Test
  public void subtractAmounts() {
    assertThat(MoneyAmount.of(1.00).subtract(1.00),
        equalTo(MoneyAmount.zero()));
    assertThat(MoneyAmount.of(1.00).subtract(0.50),
        equalTo(MoneyAmount.of(0.50)));
    assertThat(MoneyAmount.of(0.00).subtract(0.50),
        equalTo(MoneyAmount.of(-0.50)));
  }

}
