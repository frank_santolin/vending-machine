/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import machine.util.MoneyAmount;

/**
 * Unit tests for {@link VendingMachine}
 */
public class VendingMachineTest {

  private MockDisplay mockDisplay = new MockDisplay();

  private VendingMachine machine = new VendingMachine(mockDisplay);

  @Test
  public void defaultStateIsOff() {
    assertFalse(machine.isOn());
  }

  @Test
  public void turnsOn() {
    machine.turnOn();
    assertTrue(machine.isOn());
  }

  @Test
  public void turnsOff() {
    turnsOn();
    machine.turnOff();
    assertFalse(machine.isOn());
  }

  @Test
  public void defaultInsertedMoneyIsZero() {
    mockDisplay.assertDisplayShowsAmount(MoneyAmount.zero());
  }

}
