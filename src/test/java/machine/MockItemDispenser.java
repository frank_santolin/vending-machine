/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import machine.model.Item;

public final class MockItemDispenser implements ItemDispenser {

  private List<Item.Selector> items = new ArrayList<>();

  @Override
  public void accept(final Item.Selector item) {
    items.add(item);
  }

  public void clear() {
    items.clear();
  }

  public void assertIsEmpty() {
    assertTrue(items.isEmpty());
  }

  public void assertContainsItem(final Item.Selector item) {
    assertThat(items, contains(item));
  }

}
