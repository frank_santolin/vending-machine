/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine.state;

import static machine.model.Coin.FIFTY_PENCE;
import static machine.model.Coin.ONE_POUND;
import static machine.model.Coin.TEN_PENCE;
import static machine.model.Coin.TWENTY_PENCE;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import machine.model.Coin;
import machine.state.InsertedMoneyHandler;
import machine.util.MoneyAmount;

public class InsertedMoneyHandlerTest {

  private InsertedMoneyHandler insertedMoney = new InsertedMoneyHandler();

  @Test
  public void defaultStateIsZeroAmount() {
    assertEquals(MoneyAmount.zero(), insertedMoney.getAmount());
  }

  @Test
  public void acceptTenPence() {
    acceptCoins(TEN_PENCE);

    assertEquals(MoneyAmount.of("£0.10"), insertedMoney.getAmount());
  }

  @Test
  public void acceptTwentyPence() {
    acceptCoins(TWENTY_PENCE);

    assertEquals(MoneyAmount.of("£0.20"), insertedMoney.getAmount());
  }

  @Test
  public void acceptFiftyPence() {
    acceptCoins(FIFTY_PENCE);

    assertEquals(MoneyAmount.of("£0.50"), insertedMoney.getAmount());
  }

  @Test
  public void acceptOnePound() {
    acceptCoins(ONE_POUND);

    assertEquals(MoneyAmount.of("£1.00"), insertedMoney.getAmount());
  }

  @Test
  public void acceptMultipleCoins() {
    acceptCoins(ONE_POUND, TWENTY_PENCE, FIFTY_PENCE, TEN_PENCE);

    assertEquals(MoneyAmount.of("£1.80"), insertedMoney.getAmount());
  }

  @Test
  public void whenCoinReturnAmountShouldBeZero() {
    acceptCoins(ONE_POUND, TWENTY_PENCE, FIFTY_PENCE, TEN_PENCE);

    assertThat(insertedMoney.coinReturn(),
        containsInAnyOrder(ONE_POUND, TWENTY_PENCE, FIFTY_PENCE, TEN_PENCE));
    assertEquals(MoneyAmount.zero(), insertedMoney.getAmount());
  }

  @Test
  public void whenCollectCoinsAmountShouldBeZero() {
    acceptCoins(ONE_POUND);

    insertedMoney.collect();
    assertEquals(MoneyAmount.zero(), insertedMoney.getAmount());
  }

  private void acceptCoins(Coin... coins) {
    for (Coin coin : coins) {
      insertedMoney.accept(coin);
    }
  }

}
