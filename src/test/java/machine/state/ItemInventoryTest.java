/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine.state;

import static machine.model.Item.Selector.A;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import machine.model.ItemSelectionResult;
import machine.state.ItemInventory;
import machine.state.ItemNotAvailable;
import machine.util.MoneyAmount;

public class ItemInventoryTest {

  /** inventory that has one of each item */
  private ItemInventory inventory = new ItemInventory(item -> 1);

  @Test
  public void selectItemWhenNoMoneyInserted() {
    ItemSelectionResult result = inventory.getItem(A, MoneyAmount.zero());

    assertFalse(result.getItem().isPresent());
    assertFalse(result.getChange().isPresent());
    assertTrue(result.getPrice().isPresent());
  }

  @Test
  public void selectItemWhenCorrectMoneyInserted() {
    ItemSelectionResult result = inventory.getItem(A, MoneyAmount.of(0.60));

    assertTrue(result.getItem().isPresent());
    assertFalse(result.getChange().isPresent());
    assertFalse(result.getPrice().isPresent());
  }

  @Test
  public void selectItemWhenMoreMoneyInsertedThanItemPrice() {
    ItemSelectionResult result = inventory.getItem(A, MoneyAmount.of(1.00));

    assertTrue(result.getItem().isPresent());
    assertTrue(result.getChange().isPresent());
    assertFalse(result.getPrice().isPresent());

    assertThat(result.getChange().get().getValue(), equalTo(0.40));
  }

  @Test
  public void selectItemWhenMoneyAmountIsNull() {
    ItemSelectionResult result = inventory.getItem(A, null);

    assertFalse(result.getItem().isPresent());
    assertFalse(result.getChange().isPresent());
    assertTrue(result.getPrice().isPresent());
  }

  @Test(expected = ItemNotAvailable.class)
  public void selectItemWhenItemIsNotAvailable() {
    selectItemWhenCorrectMoneyInserted(); // exhaust item from vending machine

    inventory.getItem(A, MoneyAmount.of(0.60));
    fail("item should not be available");
  }

}
