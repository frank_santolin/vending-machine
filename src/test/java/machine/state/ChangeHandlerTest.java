/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine.state;

import static machine.model.Coin.FIFTY_PENCE;
import static machine.model.Coin.ONE_POUND;
import static machine.model.Coin.TEN_PENCE;
import static machine.model.Coin.TWENTY_PENCE;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.function.Function;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import machine.model.Coin;
import machine.state.ChangeHandler;
import machine.state.ChangeNotAvailable;
import machine.util.MoneyAmount;

public class ChangeHandlerTest {

  @Mock
  private Function<Coin, Integer> availableChange;

  private ChangeHandler changeHandler;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
    when(availableChange.apply(any(Coin.class))).thenReturn(1); // only one coin
                                                                // available
                                                                // each

    changeHandler = new ChangeHandler(availableChange);
  }

  @Test
  public void changeForOnePoundIsAvailable() {
    assertAvailable(ONE_POUND);
  }

  @Test
  public void changeForFiftyPenceIsAvailable() {
    assertAvailable(FIFTY_PENCE);
  }

  @Test
  public void changeForTwentyPenceIsAvailable() {
    assertAvailable(TWENTY_PENCE);
  }

  @Test
  public void changeForTenPenceIsAvailable() {
    assertAvailable(TEN_PENCE);
  }

  @Test
  public void changeForThirtyPence() {
    final List<Coin> change = changeHandler.apply(MoneyAmount.of("£0.30"));
    assertNotNull(change);
    assertThat(change, hasSize(2));
    assertThat(change, containsInAnyOrder(TWENTY_PENCE, TEN_PENCE));
  }

  @Test
  public void changeForOnePoundEightyPence() {
    final List<Coin> change = changeHandler.apply(MoneyAmount.of("£1.80"));
    assertNotNull(change);
    assertThat(change, hasSize(4));
    assertThat(change,
        containsInAnyOrder(ONE_POUND, FIFTY_PENCE, TWENTY_PENCE, TEN_PENCE));
  }

  @Test(expected = ChangeNotAvailable.class)
  public void noChangeAvailableForFortyPence() {
    changeHandler.apply(MoneyAmount.of("0.40"));
  }

  @Test(expected = ChangeNotAvailable.class)
  public void noChangeAvailableForNinetyPence() {
    changeHandler.apply(MoneyAmount.of("0.90"));
  }

  @Test(expected = ChangeNotAvailable.class)
  public void noChangeAvailableForOnePoundFortyPence() {
    changeHandler.apply(MoneyAmount.of("1.40"));
  }

  @Test(expected = ChangeNotAvailable.class)
  public void noChangeAvailableForOnePoundNinetyPence() {
    changeHandler.apply(MoneyAmount.of("1.90"));
  }

  @Test(expected = ChangeNotAvailable.class)
  public void noChangeAvailableForTwoPounds() {
    changeHandler.apply(MoneyAmount.of("2.00"));
  }

  private void assertAvailable(Coin coin) {
    final List<Coin> change = changeHandler
        .apply(MoneyAmount.of(coin.getValueAsText()));
    assertNotNull(change);
    assertThat(change, hasSize(1));
    assertThat(change, contains(coin));
  }

}
