/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine.end2end;

import static machine.model.Coin.FIFTY_PENCE;
import static machine.model.Coin.ONE_POUND;
import static machine.model.Coin.TEN_PENCE;
import static machine.model.Coin.TWENTY_PENCE;

import org.junit.Test;

import machine.MockDisplay;
import machine.VendingMachine;
import machine.model.Coin;

public class InsertMoneyTest {

  private MockDisplay mockDisplay = new MockDisplay();

  private VendingMachine machine = new VendingMachine(mockDisplay);

  @Test
  public void insertTenPenceCoin() {
    insertCoins(TEN_PENCE);

    mockDisplay.assertDisplayShowsAmount(TEN_PENCE.getValue());
  }

  @Test
  public void insertTwentyPenceCoin() {
    insertCoins(TWENTY_PENCE);

    mockDisplay.assertDisplayShowsAmount(TWENTY_PENCE.getValue());
  }

  @Test
  public void insertFiftyPenceCoin() {
    insertCoins(FIFTY_PENCE);

    mockDisplay.assertDisplayShowsAmount(FIFTY_PENCE.getValue());
  }

  @Test
  public void insertOnePoundCoin() {
    insertCoins(ONE_POUND);

    mockDisplay.assertDisplayShowsAmount(ONE_POUND.getValue());
  }

  @Test
  public void noStateChangedWhenInsertingNullInsteadOfCoin() {
    mockDisplay.show("123"); // some initial message
    machine.insert(null);

    mockDisplay.assertDisplayShows("123");
  }

  @Test
  public void insertTwoTenPenceCoins() {
    insertCoins(TEN_PENCE, TEN_PENCE);

    mockDisplay.assertDisplayShowsAmount(TWENTY_PENCE.getValue());
  }

  @Test
  public void insertThreeTenPenceCoins() {
    insertCoins(TEN_PENCE, TEN_PENCE, TEN_PENCE);

    mockDisplay.assertDisplayShowsAmount(
        TWENTY_PENCE.getValue() + TEN_PENCE.getValue());
  }

  @Test
  public void insertOnePoundSeventyPence() {
    insertCoins(ONE_POUND, TEN_PENCE, TEN_PENCE, FIFTY_PENCE);

    mockDisplay.assertDisplayShowsAmount(1.70);
  }

  private void insertCoins(final Coin... coins) {
    for (Coin coin : coins) {
      machine.insert(coin);
    }
  }

}
