/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine.end2end;

import static machine.model.Coin.FIFTY_PENCE;
import static machine.model.Coin.ONE_POUND;
import static machine.model.Coin.TEN_PENCE;

import org.junit.Test;

import machine.MockCoinDispenser;
import machine.MockDisplay;
import machine.VendingMachine;
import machine.model.Coin;
import machine.util.MoneyAmount;

public class CoinReturnTest {

  private MockDisplay mockDisplay = new MockDisplay();
  private MockCoinDispenser mockCoinDispenser = new MockCoinDispenser();

  private VendingMachine machine = 
      new VendingMachine(mockDisplay, mockCoinDispenser);

  @Test
  public void coinReturnBeforeInsertingCoins() {
    mockCoinDispenser.assertIsEmpty();

    machine.coinReturn();

    mockDisplay.assertDisplayShowsAmount(MoneyAmount.zero());
    mockCoinDispenser.assertIsEmpty();
  }

  @Test
  public void coinReturnAfterInsertingTenPence() {
    mockCoinDispenser.assertIsEmpty();
    insertCoins(TEN_PENCE);

    machine.coinReturn();

    mockDisplay.assertDisplayShowsAmount(MoneyAmount.zero());
    mockCoinDispenser.assertCointainsCoinsToTheAmountOf(TEN_PENCE.getValue());
  }

  @Test
  public void coinReturnAfterInsertingMultipleCoins() {
    insertCoins(TEN_PENCE, ONE_POUND, FIFTY_PENCE, TEN_PENCE);

    machine.coinReturn();

    mockDisplay.assertDisplayShowsAmount(MoneyAmount.zero());
    mockCoinDispenser.assertCointainsCoinsToTheAmountOf(1.70);
  }

  private void insertCoins(final Coin... coins) {
    for (Coin coin : coins) {
      machine.insert(coin);
    }
  }

}
