/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine.end2end;

import static machine.Display.formatChangeNotAvailable;
import static machine.Display.formatItemNotAvailable;
import static machine.Display.formatPleaseInsertAmount;
import static machine.model.Coin.FIFTY_PENCE;
import static machine.model.Coin.ONE_POUND;
import static machine.model.Coin.TEN_PENCE;
import static machine.model.Item.Selector.A;
import static machine.model.Item.Selector.B;
import static machine.model.Item.Selector.C;

import org.junit.Test;

import machine.MockCoinDispenser;
import machine.MockDisplay;
import machine.MockItemDispenser;
import machine.VendingMachine;
import machine.model.Coin;
import machine.util.MoneyAmount;

public class ItemSelectionTest {

  private MockDisplay mockDisplay = new MockDisplay();
  private MockCoinDispenser mockCoinDispenser = new MockCoinDispenser();
  private MockItemDispenser mockItemDispenser = new MockItemDispenser();

  private VendingMachine machine = 
      new VendingMachine(mockDisplay, mockCoinDispenser, mockItemDispenser, 
        coin -> 2, // change available for each coin
        item -> 1); // test with one item available each

  @Test
  public void selectItemABeforeInsertingMoney() {
    mockDisplay.assertDisplayShowsAmount(MoneyAmount.zero());
    mockItemDispenser.assertIsEmpty();

    machine.getA();

    mockDisplay.assertDisplayShows(formatPleaseInsertAmount(0.60));
    mockItemDispenser.assertIsEmpty();
  }

  @Test
  public void selectItemAWhenInsufficientMoneyInserted() {
    insertCoins(FIFTY_PENCE);
    mockDisplay.assertDisplayShowsAmount(FIFTY_PENCE.getValue());
    mockItemDispenser.assertIsEmpty();

    machine.getA();

    mockDisplay.assertDisplayShows(formatPleaseInsertAmount(0.60));
    mockDisplay.assertDisplayShowsAmount(FIFTY_PENCE.getValue());
    mockItemDispenser.assertIsEmpty();
  }

  @Test
  public void selectItemAWhenCorrectMoneyInserted() {
    insertCoins(FIFTY_PENCE, TEN_PENCE);
    mockDisplay.assertDisplayShowsAmount(0.60);
    mockItemDispenser.assertIsEmpty();

    machine.getA();

    mockDisplay.assertDisplayShowsAmount(0.00);
    mockCoinDispenser.assertIsEmpty();
    mockItemDispenser.assertContainsItem(A);
  }

  @Test
  public void selectItemAWhenMoreMoneyInsertedThanItemPrice() {
    insertCoins(FIFTY_PENCE, FIFTY_PENCE);
    mockDisplay.assertDisplayShowsAmount(1.00);
    mockItemDispenser.assertIsEmpty();

    machine.getA();

    mockDisplay.assertDisplayShowsAmount(0.00);
    mockCoinDispenser.assertCointainsCoinsToTheAmountOf(0.40);
    mockItemDispenser.assertContainsItem(A);
  }

  @Test
  public void selectItemBWhenCorrectMoneyInserted() {
    insertCoins(ONE_POUND);
    mockDisplay.assertDisplayShowsAmount(1.00);
    mockItemDispenser.assertIsEmpty();

    machine.getB();

    mockDisplay.assertDisplayShowsAmount(0.00);
    mockCoinDispenser.assertIsEmpty();
    mockItemDispenser.assertContainsItem(B);
  }

  @Test
  public void selectItemCWhenCorrectMoneyInserted() {
    insertCoins(ONE_POUND, FIFTY_PENCE, TEN_PENCE, TEN_PENCE);
    mockDisplay.assertDisplayShowsAmount(1.70);
    mockItemDispenser.assertIsEmpty();

    machine.getC();

    mockDisplay.assertDisplayShowsAmount(0.00);
    mockCoinDispenser.assertIsEmpty();
    mockItemDispenser.assertContainsItem(C);
  }

  @Test
  public void selectItemAWhenItemNotAvailable() {
    selectItemAWhenCorrectMoneyInserted(); // exhaust item from vending machine
    mockItemDispenser.clear();
    insertCoins(ONE_POUND);

    machine.getA();

    mockDisplay.assertDisplayShows(formatItemNotAvailable(A.toString()));
    mockDisplay.assertDisplayShowsAmount(1.00);
    mockItemDispenser.assertIsEmpty();
  }

  @Test
  public void selectItemAWhenChangeNotAvailable() {
    machine = new VendingMachine(mockDisplay, mockCoinDispenser,
        mockItemDispenser, coin -> 0, // no change available for each coin
        item -> 1); // test with one item available each
    insertCoins(ONE_POUND);

    machine.getA();

    mockDisplay.assertDisplayShows(formatChangeNotAvailable(0.40));
    mockDisplay.assertDisplayShowsAmount(0.00);
    mockCoinDispenser.assertIsEmpty();
    mockItemDispenser.assertContainsItem(A);
  }

  private void insertCoins(final Coin... coins) {
    for (Coin coin : coins) {
      machine.insert(coin);
    }
  }

}
