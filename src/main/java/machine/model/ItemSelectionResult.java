/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine.model;

import java.util.Optional;

import machine.util.MoneyAmount;

public class ItemSelectionResult {

  private final Optional<Item.Selector> item;
  private final Optional<MoneyAmount> change;
  private final Optional<MoneyAmount> price;

  public static ItemSelectionResult insertCorrectAmount(
      final MoneyAmount price) {
    return new ItemSelectionResult(Optional.empty(), Optional.empty(),
        Optional.of(price));
  }

  public static ItemSelectionResult returnItem(final Item.Selector selector) {
    return new ItemSelectionResult(Optional.of(selector), Optional.empty(),
        Optional.empty());
  }

  public static ItemSelectionResult returnItemWithChange(
      final Item.Selector selector, final MoneyAmount change) {
    return new ItemSelectionResult(Optional.of(selector), Optional.of(change),
        Optional.empty());
  }

  private ItemSelectionResult(final Optional<Item.Selector> item,
      final Optional<MoneyAmount> change, final Optional<MoneyAmount> price) {
    this.item = item;
    this.change = change;
    this.price = price;
  }

  /**
   * Return item selector (A, B, or C) if enough money was inserted.
   */
  public Optional<Item.Selector> getItem() {
    return item;
  }

  /**
   * Return change money amount due if more money was inserted than the item's
   * price.
   */
  public Optional<MoneyAmount> getChange() {
    return change;
  }

  /**
   * Return the item's price if not enough money was inserted.
   */
  public Optional<MoneyAmount> getPrice() {
    return price;
  }

}