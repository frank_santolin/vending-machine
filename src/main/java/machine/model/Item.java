/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine.model;

/**
 * Mutable item object which must hold the available item count.
 */
public final class Item {

  public enum Selector {
    A, B, C
  }

  private final Selector selector;
  private final double price;
  private int available;

  public Item(final Selector selector, final double price,
      final int available) {
    this.selector = selector;
    this.price = price;
    this.available = available;
  }

  public Selector getSelector() {
    return selector;
  }

  public double getPrice() {
    return price;
  }

  public boolean isNotAvailable() {
    return available == 0;
  }

  public void remove() {
    available--;
  }

}
