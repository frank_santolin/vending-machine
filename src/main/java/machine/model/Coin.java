/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine.model;

import java.math.BigDecimal;
import java.util.List;

public enum Coin {

  TEN_PENCE("0.10"), 
  TWENTY_PENCE("0.20"), 
  FIFTY_PENCE("0.50"), 
  ONE_POUND("1.00");

  private final String valueAsText;

  private Coin(final String valueAsText) {
    this.valueAsText = valueAsText;
  }

  public String getValueAsText() {
    return valueAsText;
  }

  public Double getValue() {
    return Double.valueOf(valueAsText);
  }

  public static int highToLowComparator(final Coin c1, final Coin c2) {
    return getValueAsAnInt(c2) - getValueAsAnInt(c1);
  }

  private static int getValueAsAnInt(final Coin coin) {
    return Double.valueOf(coin.getValueAsText().replace(".", "")).intValue();
  }

  public static Double sum(final List<Coin> coins) {
    final long result = Math.round((coins.stream().mapToDouble(Coin::getValue).sum()) * 100);
    return BigDecimal.valueOf(result, 2).doubleValue();
  }

}
