/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine.state;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import machine.model.Coin;
import machine.util.MoneyAmount;

public class ChangeHandler implements Function<MoneyAmount, List<Coin>> {

  private class AvailableCounter {

    private int count;

    AvailableCounter(final int startingCount) {
      this.count = startingCount;
    }

    int getAndDecrement(final int requested) {
      final int required = requested <= count ? requested : count;
      count = count - required;
      return required;
    }
  }

  private Map<Coin, AvailableCounter> availableChange;

  public ChangeHandler(final Function<Coin, Integer> availableChange) {
    this.availableChange = Arrays.stream(Coin.values()).collect(
        toMap(c -> c, c -> new AvailableCounter(availableChange.apply(c))));
  }

  @Override
  public List<Coin> apply(MoneyAmount moneyAmount) {
    MoneyAmountToCoinsContext context = 
        new MoneyAmountToCoinsContext(moneyAmount);
    context = convertMoneyAmountToCoins(context);
    if (context.hasMoreMoneyAmountToBeConverted()) {
      throw new ChangeNotAvailable();
    }
    return context.coins;
  }

  private class MoneyAmountToCoinsContext {

    private BigDecimal outstandingValue;
    private List<Coin> coins = new ArrayList<>();

    MoneyAmountToCoinsContext(final MoneyAmount moneyAmount) {
      this.outstandingValue = BigDecimal.valueOf(moneyAmount.getValue());
    }

    boolean hasMoreMoneyAmountToBeConverted() {
      return outstandingValue.compareTo(BigDecimal.ZERO) > 0;
    }

  }

  /**
   * This implementation is not optimised. Uses a naive algorithm to work out
   * which coins to use starting from the highest coin denominations first (£1,
   * £0.50, £0.20 and then £0.10)
   */
  private MoneyAmountToCoinsContext convertMoneyAmountToCoins(
      final MoneyAmountToCoinsContext initialContext) {
    MoneyAmountToCoinsContext context = initialContext;
    for (Coin coin : highToLowCoinDenominators()) {
      context = applyCoinDenominatorConversion(context, coin);
    }
    return context;
  }

  private MoneyAmountToCoinsContext applyCoinDenominatorConversion(
      final MoneyAmountToCoinsContext context, final Coin coin) {

    BigDecimal denominationValue = 
        new BigDecimal(coin.getValue(), new MathContext(2));
    BigDecimal[] qandr = context.outstandingValue
        .divideAndRemainder(denominationValue);
    int numberOfCoins = calcNumberOfCoins(qandr, coin);

    for (int i = 0; i < numberOfCoins; i++) {
      context.coins.add(coin);
      context.outstandingValue = context.outstandingValue
          .subtract(denominationValue);
    }

    return context;
  }

  private int calcNumberOfCoins(final BigDecimal[] qandr, final Coin coin) {
    final int numberOfCoinsRequired = qandr[0].intValue();
    if (numberOfCoinsRequired < 1) {
      return 0;
    }

    return availableChange.get(coin).getAndDecrement(numberOfCoinsRequired);
  }

  private static List<Coin> highToLowCoinDenominators() {
    return Arrays.stream(Coin.values()).sorted(Coin::highToLowComparator)
        .collect(toList());
  }

}
