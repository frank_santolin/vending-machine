/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine.state;

import static java.util.stream.Collectors.toMap;
import static machine.model.Item.Selector.A;
import static machine.model.Item.Selector.B;
import static machine.model.Item.Selector.C;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;

import machine.model.Item;
import machine.model.ItemSelectionResult;
import machine.util.MoneyAmount;

public class ItemInventory {

  private final Map<Item.Selector, Item> items;

  public ItemInventory(final Function<Item.Selector, Integer> itemCount) {
    this.items = Arrays
        .asList(new Item(A, 0.60, itemCount.apply(A)),
            new Item(B, 1.00, itemCount.apply(B)),
            new Item(C, 1.70, itemCount.apply(C)))
        .stream().collect(toMap(Item::getSelector, i -> i));
  }

  public ItemSelectionResult getItem(final Item.Selector selector,
      final MoneyAmount insertedAmount) {

    final Item item = items.get(selector);
    if (item.isNotAvailable()) {
      throw new ItemNotAvailable();
    }

    final double price = item.getPrice();

    if (insertedAmount == null || insertedAmount.isLessThan(price)) {
      return ItemSelectionResult.insertCorrectAmount(MoneyAmount.of(price));
    }

    item.remove();

    final MoneyAmount change = insertedAmount.subtract(price);
    return change.equals(MoneyAmount.zero())
        ? ItemSelectionResult.returnItem(selector)
        : ItemSelectionResult.returnItemWithChange(selector, change);
  }

}
