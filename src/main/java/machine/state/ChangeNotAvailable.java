/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine.state;

public class ChangeNotAvailable extends RuntimeException {

  private static final long serialVersionUID = 1L;

}
