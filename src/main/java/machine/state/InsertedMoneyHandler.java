/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine.state;

import static java.util.stream.Collectors.toList;
import static machine.model.Coin.sum;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

import machine.model.Coin;
import machine.util.MoneyAmount;

public class InsertedMoneyHandler
    implements Supplier<MoneyAmount>, Consumer<Coin> {

  private final List<Coin> coins = new ArrayList<>();

  public MoneyAmount getAmount() {
    return get();
  }

  @Override
  public MoneyAmount get() {
    return MoneyAmount.of(sum(coins));
  }

  @Override
  public void accept(Coin coin) {
    coins.add(coin);
  }

  public List<Coin> coinReturn() {
    final List<Coin> result = coins.stream().collect(toList());
    coins.clear();
    return result;
  }

  public void collect() {
    coins.clear(); // pretend coins drop in coin box
  }

}
