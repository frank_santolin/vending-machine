/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine;

/**
 * Simple Display type consuming messages of any format which does not prescribe
 * a particular layout.
 */
@FunctionalInterface
public interface Display {

  static String formatInsertedMoneyAmount(double amount) {
    return String.format("Amount: £%.2f", amount);
  }

  static String formatPleaseInsertAmount(double amount) {
    return String.format("Please insert £%.2f", amount);
  }

  static String formatItemNotAvailable(String itemName) {
    return String.format("Item %s not available", itemName);
  }

  static String formatChangeNotAvailable(double amount) {
    return String.format("Change %.2f not available", amount);
  }

  void accept(String message);

}
