/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine;

import machine.model.Coin;

@FunctionalInterface
public interface CoinSlot {

  void insert(Coin coin);

}
