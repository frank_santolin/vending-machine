/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine;

import machine.model.Item;

@FunctionalInterface
public interface ItemDispenser {

  void accept(Item.Selector item);

}
