/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine.util;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Models money amounts including pence.
 */
public class MoneyAmount {

  private static final String REGEX = "^\\s*£?(-?\\d+.\\d+)\\s*$";

  public static MoneyAmount zero() {
    return new MoneyAmount(Double.valueOf("0.00"));
  }

  public static MoneyAmount of(final Double value) {
    return new MoneyAmount(value);
  }

  public static MoneyAmount of(final String valueAsText) {
    Pattern p = Pattern.compile(REGEX);
    Matcher m = p.matcher(valueAsText == null ? "" : valueAsText);
    if (m.find()) {
      return new MoneyAmount(Double.parseDouble(m.group(1)));
    }

    return new MoneyAmount(); // no match -> empty amount
  }

  private final Optional<Double> value;

  /** invalid money amount */
  private MoneyAmount() {
    this(null);
  }

  private MoneyAmount(final Double value) {
    this.value = value == null ? Optional.empty() : Optional.of(value);
  }

  public Double getValue() {
    return value.orElse(Double.NaN);
  }

  public boolean isLessThan(final Double value) {
    return value != null && this.getValue().compareTo(value) < 0;
  }

  public MoneyAmount subtract(final double other) {
    return MoneyAmount.of(this.getValue() - other);
  }

  @Override
  public int hashCode() {
    return this.getValue().hashCode();
  }

  @Override
  public boolean equals(final Object obj) {
    return obj != null
        && this.getClass() == obj.getClass()
        && ((MoneyAmount) obj).getValue().equals(this.getValue());
  }

  @Override
  public String toString() {
    return String.format("£%.2f", getValue());
  }

}
