/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine;

import java.util.List;

import machine.model.Coin;

@FunctionalInterface
public interface CoinDispenser {

  void accept(List<Coin> coins);

}
