/*
 * Copyright (c) 2017 Frank Santolin
 *
 * This code is under MIT licence. See the LICENSE file in the project root for license terms. 
 */
package machine;

import static machine.Display.formatChangeNotAvailable;
import static machine.Display.formatInsertedMoneyAmount;
import static machine.Display.formatItemNotAvailable;
import static machine.Display.formatPleaseInsertAmount;
import static machine.MachineStatus.OFF;
import static machine.MachineStatus.ON;
import static machine.model.Item.Selector.A;
import static machine.model.Item.Selector.B;
import static machine.model.Item.Selector.C;

import java.util.Optional;
import java.util.function.Function;

import machine.model.Coin;
import machine.model.Item;
import machine.model.ItemSelectionResult;
import machine.state.ChangeHandler;
import machine.state.ChangeNotAvailable;
import machine.state.InsertedMoneyHandler;
import machine.state.ItemInventory;
import machine.state.ItemNotAvailable;
import machine.util.MoneyAmount;

/**
 * Encapsulates the state of a vending machine and the operations that can be
 * performed on it
 */
public class VendingMachine implements ItemSelection, CoinSlot, CoinReturn {

  /* vending machine state */
  private MachineStatus status = OFF;
  private final ItemInventory inventory;
  private final ChangeHandler changeHandler;
  private final InsertedMoneyHandler insertedMoney;

  /* vending machine output */
  private final Display display;
  private final CoinDispenser coinDispenser;
  private final ItemDispenser itemDispenser;

  public VendingMachine() {
    this(t -> {
      /* no-op default display */ });
  }

  public VendingMachine(final Display display) {
    this(display, t -> {
      /* no-op default coin dispenser */ });
  }

  public VendingMachine(final Display display,
      final CoinDispenser coinDispenser) {
    this(display, coinDispenser, t -> {
      /* no-op default item dispenser */ });
  }

  public VendingMachine(final Display display,
      final CoinDispenser coinDispenser, final ItemDispenser itemDispenser) {
    this(display, coinDispenser, itemDispenser, 
        coin -> 10, // fill coin box with 10 of each denomination
        item -> 10); // fill inventory with 10 of each item
  }

  public VendingMachine(final Display display,
      final CoinDispenser coinDispenser, final ItemDispenser itemDispenser,
      final Function<Coin, Integer> availableChange,
      final Function<Item.Selector, Integer> availableItems) {
    this.display = display;
    this.coinDispenser = coinDispenser;
    this.itemDispenser = itemDispenser;
    this.changeHandler = new ChangeHandler(availableChange);
    this.inventory = new ItemInventory(availableItems);
    this.insertedMoney = new InsertedMoneyHandler();
    renderDisplay();
  }

  private void renderDisplay() {
    renderDisplayWithWarning(Optional.empty());
  }

  private void renderDisplayWithWarning(final Optional<String> warning) {
    display.accept(String.join(" ", warning.orElse(""),
        formatInsertedMoneyAmount(insertedMoney.getAmount().getValue())));
  }

  public boolean isOn() {
    return ON.equals(this.status);
  }

  public void turnOn() {
    this.status = ON;
  }

  public void turnOff() {
    this.status = OFF;
  }

  @Override
  public void getA() {
    getItem(A);
  }

  @Override
  public void getB() {
    getItem(B);
  }

  @Override
  public void getC() {
    getItem(C);
  }

  private void getItem(final Item.Selector selector) {
    try {
      final ItemSelectionResult result = 
          inventory.getItem(selector, insertedMoney.getAmount());
      result.getItem().ifPresent(this::collectMoneyAndDispenseItem);
      result.getChange().ifPresent(this::dispenseChange);
      result.getPrice().ifPresent(this::showMessageToInsertMoney);
    } catch (ItemNotAvailable notAvailable) {
      renderDisplayWithWarning(
          Optional.of(formatItemNotAvailable(selector.toString())));
    }
  }

  private void collectMoneyAndDispenseItem(final Item.Selector item) {
    insertedMoney.collect();
    itemDispenser.accept(item);
    renderDisplay();
  }

  private void dispenseChange(final MoneyAmount amount) {
    try {
      coinDispenser.accept(changeHandler.apply(amount));
    } catch (ChangeNotAvailable notAvailable) {
      renderDisplayWithWarning(
          Optional.of(formatChangeNotAvailable(amount.getValue())));
    }
  }

  private void showMessageToInsertMoney(final MoneyAmount price) {
    renderDisplayWithWarning(
        Optional.of(formatPleaseInsertAmount(price.getValue())));
  }

  @Override
  public void insert(final Coin coin) {
    if (coin != null) {
      insertedMoney.accept(coin);
      renderDisplay();
    }
  }

  @Override
  public void coinReturn() {
    coinDispenser.accept(insertedMoney.coinReturn());
    renderDisplay();
  }

}
