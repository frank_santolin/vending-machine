# Vending Machine Exercise

This is the result of an exercise to model a vending machine.

---
## Prerequisites
- Java 8
- Maven 3.3.x

---
## Run Tests

You can run the tests with Maven:

		mvn test
		
---
## Overview
The solution comprises of a main implementation class *VendingMachine* that
maintains state during its operation. It acts as a controller accepting user
requests/actions through its *ItemSelection*, *CoinSlot* and *CoinReturn* input
interfaces, and execute the appropriate logic before responding using any of
its output interfaces, such as *Display*, *ItemDispenser* and *CoinDispenser*.

The vending machine solution models its state using four types: *MachineStatus*,
*ItemInventory*, *ChangeHandler* and *InsertedMoneyHandler*.

The *Coin* and *Item* entities represent the money that's been inserted
and the item that's been selected by the user. The same entity types also represent
any money change due and the dispensed item by the machine.

Internally, collections of *Coin*s are translated into *MoneyAmount* 
representations, and vice-versa.

*ItemSelectionResult* is used internally to represent the outcome of processing
the user's item selection.

---
## Maven
The maven POM specifies the *junit*, *hamcrest-library* and *mockito-core*
test dependencies to ensure that all of the libraries needed are available 
for this project.

---
## Code Structure
```
machine                            vending machine abstract types
       |
       .model                      entity objects
       |
       .state                      vending machine state types 
       |
       .util                       utility classes
```

---
## Decisions and Assumptions
Given that this is a practical assessment no great considerations was given to design
and algorithm optimisations. The main focus has been on code readability.

I have chosen to write code in a declarative style as much as possible.
Java 8 language features allows this through its uses of the Stream API and
support for functional style programming, e.g. lambda expressions. The end
result is more concise and easily maintainable code.

I have used, in particular, the *Single Responsibility* and
*Interface Segregation* principles (see SOLID) to guide a better 
solution design. I have used *abstraction* and *encapsulation* to guide
a more modular design by writing tests that focus on observable behaviour
rather than just querying the objects for some data and then deciding what 
to do (i.e. "Tell, Don't Ask").
